<?php
class CategoriaDAO{
    private $idCategoria;
    private $nombre;
    private $estado;
    
    function CategoriaDAO ($pIdCategoria, $pNombre, $pEstado) {
        $this -> idCategoria = $pIdCategoria;
        $this -> nombre = $pNombre;
        $this -> estado = $pEstado;
    }
    
    function consultar () {
        return "select nombre, estado
                from categoria
                where idCategoria = '" . $this -> idCategoria . "'";
    }
    
    function crear () {
        return "insert into categoria (nombre,estado)
                values ('" . $this -> nombre . "', '" . $this -> estado . "')";                
    }
    
    function consultarTodos () {
        return "select idCategoria, nombre, estado
                from categoria";
    }

    function verificar(){
        return "select * 
        from categoria 
        where nombre='" . $this -> nombre . "'";
    }

    function mostrarTodos () {
        return "select idCategoria, nombre, estado
                from Categoria
                where idCategoria = '" . $this -> idCategoria . "'";
    }
    
    function editar () {
        return "update categoria 
                set nombre = '" . $this -> nombre . "'
                where idCategoria = '" . $this -> idCategoria . "'";
    }

    function editarEstado () {
        return "update categoria 
                set estado = '" . $this -> estado . "'
                where idCategoria = '" . $this -> idCategoria . "'";
    }
    
    function consultarPorPagina ($cantidad, $pagina, $orden, $dir) {
        if($orden == "" || $dir == ""){
            return "select idCategoria, nombre, estado
                from Categoria
                limit " . strval(($pagina - 1) * $cantidad) . ", " . $cantidad;            
        }else{
            return "select idCategoria, nombre, estado
                from Categoria
                order by " . $orden . " " . $dir . "
                limit " . strval(($pagina - 1) * $cantidad) . ", " . $cantidad;            
        }
    }
    
    function consultarTotalRegistros () {
        return "select count(idCategoria)
                from Categoria";
    }

    function buscar($filtro){
        return "select idCategoria, nombre, estado
                from Categoria
                where nombre like '" . $filtro . "%' or apellido like '" . $filtro . "%'";
    }
}

?>