<?php
class ClienteDAO
{
    private $idCliente;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $estado;
    private $foto;

    function ClienteDAO($pIdCliente, $pNombre, $pApellido, $pCorreo, $pClave, $pEstado, $pFoto)
    {
        $this->idCliente = $pIdCliente;
        $this->nombre = $pNombre;
        $this->apellido = $pApellido;
        $this->correo = $pCorreo;
        $this->clave = $pClave;
        $this->estado = $pEstado;
        $this->foto = $pFoto;
    }

    function autenticar()
    {
        return "select idCliente, estado
                from cliente
                where correo = '" . $this->correo . "' and clave = sha1('" . $this->clave . "')";
    }

    function crear()
    {
        return "insert into cliente (nombre,apellido,correo,clave,estado)
                values ('" . $this->nombre . "', '" . $this->apellido . "', '" . $this->correo . "', '" . sha1($this->clave) . "', '" . $this->estado . "')";
    }

    function verificar()
    {
        return "select * 
        from cliente 
        where correo='" . $this->correo . "'";
    }

    public function editar()
    {
        return "update cliente
                set nombre = '" . $this->nombre . "', apellido = '" . $this->apellido .
            "', correo = '" . $this->correo .
            "' where idCliente = '" . $this->idCliente .  "'";
    }

    public function editarClave()
    {
        return "update cliente
                set clave = '" .  sha1($this->clave)  .
            "' where idCliente = '" . $this->idCliente .  "'";
    }

    function consultarTodos()
    {
        return "select idCliente, nombre, apellido, correo,clave, estado, foto
                from cliente";
    }

    function consultar()
    {
        return "select idCliente, nombre, apellido, correo, estado, foto,clave
                from cliente
                where idCliente = '" . $this->idCliente . "'";
    }

    function editarEstado()
    {
        return "update cliente 
                set estado = '" . $this->estado . "'
                where idCliente = '" . $this->idCliente . "'";
    }

    function editarFoto()
    {
        return "update cliente set foto = '" . $this->foto . "'
                where idCliente = '" . $this->idCliente . "'";
    }

    public function eliminarFoto()
    {
        return "update cliente
                set foto = '' where idCliente = '" . $this->idCliente .  "'";
    }

    function consultarPorCorreo()
    {
        return "select idCliente, nombre, apellido
                from Cliente
                where correo = '" . $this->correo . "'";
    }

    function cambiarClave($id, $nuevaClave)
    {
        return "update Cliente set clave = sha1('" . $nuevaClave . "')
                where idCliente = '" . $id . "'";
    }
}
