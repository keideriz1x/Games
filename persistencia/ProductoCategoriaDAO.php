<?php
class ProductoCategoriaDAO{
    private $Producto_idProducto;
    private $Categoria_idCategoria;
    
    function ProductoCategoriaDAO ($pProducto_idProducto, $pCategoria_idCategoria) {
        $this -> Producto_idProducto = $pProducto_idProducto;
        $this -> Categoria_idCategoria = $pCategoria_idCategoria;
    }
    
    function consultar () {
        return "select Producto_idProducto, Categoria_idCategoria
                from producto_has_categoria
                where Producto_idProducto = '" . $this -> Producto_idProducto . "'";
    }
    
    function crear () {
        return "insert into producto_has_categoria (Producto_idProducto,Categoria_idCategoria)
                values ('" . $this -> Producto_idProducto . "', '" . $this -> Categoria_idCategoria . "')";                
    }
    
    function consultarTodos () {
        return "select Producto_idProducto, Categoria_idCategoria
                from producto_has_categoria
                where Producto_idProducto = '" . $this -> Producto_idProducto . "'";
    }
    
    function editar () {
        return "update ProductoCategoria 
                set Producto_idProducto = '" . $this -> Producto_idProducto . "'
                where idProductoCategoria = '" . $this -> idProductoCategoria . "'";
    }

    function editarCategoria_idCategoria () {
        return "update ProductoCategoria 
                set Categoria_idCategoria = '" . $this -> Categoria_idCategoria . "'
                where idProductoCategoria = '" . $this -> idProductoCategoria . "'";
    }
    
    function consultarPorPagina ($cantidad, $pagina, $orden, $dir) {
        if($orden == "" || $dir == ""){
            return "select idProductoCategoria, Producto_idProducto, Categoria_idCategoria
                from ProductoCategoria
                limit " . strval(($pagina - 1) * $cantidad) . ", " . $cantidad;            
        }else{
            return "select idProductoCategoria, Producto_idProducto, Categoria_idCategoria
                from ProductoCategoria
                order by " . $orden . " " . $dir . "
                limit " . strval(($pagina - 1) * $cantidad) . ", " . $cantidad;            
        }
    }
    
    function consultarTotalRegistros () {
        return "select count(idProductoCategoria)
                from ProductoCategoria";
    }

    function buscar($filtro){
        return "select idProductoCategoria, Producto_idProducto, Categoria_idCategoria
                from ProductoCategoria
                where Producto_idProducto like '" . $filtro . "%' or apellido like '" . $filtro . "%'";
    }
}

?>