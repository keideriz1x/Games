<?php
class InspectorDAO
{
    private $idInspector;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;

    function InspectorDAO($pIdInspector, $pNombre, $pApellido, $pCorreo, $pClave, $pFoto, $pEstado)
    {
        $this->idInspector = $pIdInspector;
        $this->nombre = $pNombre;
        $this->apellido = $pApellido;
        $this->correo = $pCorreo;
        $this->clave = $pClave;
        $this->foto = $pFoto;
        $this->estado = $pEstado;
    }

    function autenticar()
    {
        return "select idInspector, estado
                from inspector
                where correo = '" . $this->correo . "' and clave = sha1('" . $this->clave . "')";
    }

    function crear()
    {
        return "insert into inspector (nombre,apellido,correo,clave,estado)
                values ('" . $this->nombre . "', '" . $this->apellido . "', '" . $this->correo . "', '" . sha1($this->clave) . "', '" . $this->estado . "')";
    }

    function consultarTodos()
    {
        return "select idInspector, nombre, apellido, correo,clave, foto, estado
                from Inspector";
    }

    function consultar()
    {
        return "select nombre, apellido, correo, estado, foto,clave
                from inspector
                where idInspector = '" . $this->idInspector . "'";
    }

    function verificar()
    {
        return "select * 
        from inspector 
        where correo='" . $this->correo . "'";
    }

    public function editar(){
        return "update inspector
                set nombre = '" . $this -> nombre . "', apellido = '" . $this -> apellido . 
                "', correo = '" . $this -> correo . 
                "' where idInspector = '" . $this -> idInspector .  "'";
    }

    public function editarClave(){
        return "update inspector
                set clave = '" .  sha1($this -> clave)  . 
                "' where idInspector = '" . $this -> idInspector .  "'";
    }

    function editarEstado()
    {
        return "update Inspector 
                set estado = '" . $this->estado . "'
                where idInspector = '" . $this->idInspector . "'";
    }

    function editarFoto()
    {
        return "update inspector set foto = '" . $this->foto . "'
                where idInspector = '" . $this->idInspector . "'";
    }

    public function eliminarFoto(){
        return "update inspector
                set foto = '' where idInspector = '" . $this -> idInspector .  "'";
    }

    function consultarPorCorreo()
    {
        return "select idInspector, nombre, apellido, correo
                from Inspector
                where correo = '" . $this->correo . "'";
    }

    function cambiarClave($id, $nuevaClave)
    {
        return "update Inspector set clave = sha1('" . $nuevaClave . "')
                where idInspector = '" . $id . "'";
    }
}
