<?php
class TipoDAO{
    private $idTipo;
    private $nombre;
    private $estado;
    
    function TipoDAO ($pIdTipo, $pNombre, $pEstado) {
        $this -> idTipo = $pIdTipo;
        $this -> nombre = $pNombre;
        $this -> estado = $pEstado;
    }
    
    function consultar () {
        return "select nombre, estado
                from Tipo
                where idTipo = '" . $this -> idTipo . "'";
    }
    
    function crear () {
        return "insert into tipo (nombre,estado)
                values ('" . $this -> nombre . "', '" . $this -> estado . "')";                
    }
    
    function consultarTodos () {
        return "select idTipo, nombre, estado
                from tipo";
    }

    function verificar(){
        return "select * 
        from tipo 
        where nombre='" . $this -> nombre . "'";
    }

    function mostrarTodos () {
        return "select idTipo, nombre, estado
                from tipo
                where idTipo = '" . $this -> idTipo . "'";
    }
    
    function editar () {
        return "update Tipo 
                set nombre = '" . $this -> nombre . "'
                where idTipo = '" . $this -> idTipo . "'";
    }

    function editarEstado () {
        return "update Tipo 
                set estado = '" . $this -> estado . "'
                where idTipo = '" . $this -> idTipo . "'";
    }
    
    function consultarPorPagina ($cantidad, $pagina, $orden, $dir) {
        if($orden == "" || $dir == ""){
            return "select idTipo, nombre, estado
                from Tipo
                limit " . strval(($pagina - 1) * $cantidad) . ", " . $cantidad;            
        }else{
            return "select idTipo, nombre, estado
                from Tipo
                order by " . $orden . " " . $dir . "
                limit " . strval(($pagina - 1) * $cantidad) . ", " . $cantidad;            
        }
    }
    
    function consultarTotalRegistros () {
        return "select count(idTipo)
                from Tipo";
    }

    function buscar($filtro){
        return "select idTipo, nombre, estado
                from Tipo
                where nombre like '" . $filtro . "%' or apellido like '" . $filtro . "%'";
    }
}

?>