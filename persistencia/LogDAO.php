<?php
class LogDAO{
    private $idLog;
    private $accion;
    private $datos;
    private $fechayhora;
    private $ip;
    private $os;
    private $navegador;
    private $Administrador_idAdministrador;
    
    function LogDAO ($pIdLog, $pAccion, $pDatos, $pFechayhora, $pIp, $pOs, $pNavegador, $pAdministrador_idAdministrador) {
        $this -> idLog = $pIdLog;
        $this -> accion = $pAccion;
        $this -> datos = $pDatos;
        $this -> fechayhora = $pFechayhora;
        $this -> ip = $pIp;
        $this -> os = $pOs;
        $this -> navegador = $pNavegador;
        $this -> Administrador_idAdministrador = $pAdministrador_idAdministrador;
    }
    
    function consultar () {
        return "select idLog, accion, datos, fechayhora, ip, os
                from Log
                where idLog = '" . $this -> idLog . "'";
    }

    function consultarPoraccion () {
        return "select idLog
                from Log
                where accion = '" . $this -> accion . "'";
    }
    
    function crear () {
        return "insert into log (accion,datos,fechayhora,ip,os,navegador,Administrador_idAdministrador)
                values ('" . $this -> accion . "', '" . $this -> datos . "', '" . $this -> fechayhora . "', '" . $this -> ip . "', '" . $this -> os . "', '" . $this -> navegador . "', '" . $this -> Administrador_idAdministrador . "')";                
    }
    
    function consultarTodos () {
        return "select idLog, accion, datos, fechayhora, ip, os,navegador
                from Log";
    }
    
    function editar () {
        return "update Log 
                set accion = '" . $this -> accion . "'
                where idLog = '" . $this -> idLog . "'";
    }

    function editarEstado () {
        return "update Log 
                set estado = '" . $this -> estado . "'
                where idLog = '" . $this -> idLog . "'";
    }
    
    function consultarPorPagina ($fechayhora, $pagina, $orden, $dir) {
        if($orden == "" || $dir == ""){
            return "select idLog, accion, estado
                from Log
                limit " . strval(($pagina - 1) * $fechayhora) . ", " . $fechayhora;            
        }else{
            return "select idLog, accion, estado
                from Log
                order by " . $orden . " " . $dir . "
                limit " . strval(($pagina - 1) * $fechayhora) . ", " . $fechayhora;            
        }
    }
    
    function consultarTotalRegistros () {
        return "select count(idLog)
                from Log";
    }

    function buscar($filtro){
        return "select idLog, accion, estado
                from Log
                where accion like '" . $filtro . "%' or apellido like '" . $filtro . "%'";
    }
}
