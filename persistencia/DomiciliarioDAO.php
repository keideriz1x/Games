<?php
class DomiciliarioDAO
{
    private $idDomiciliario;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;

    function DomiciliarioDAO($pIdDomiciliario, $pNombre, $pApellido, $pCorreo, $pClave, $pFoto, $pEstado)
    {
        $this->idDomiciliario = $pIdDomiciliario;
        $this->nombre = $pNombre;
        $this->apellido = $pApellido;
        $this->correo = $pCorreo;
        $this->clave = $pClave;
        $this->foto = $pFoto;
        $this->estado = $pEstado;
    }

    function autenticar()
    {
        return "select idDomiciliario, estado
                from domiciliario
                where correo = '" . $this->correo . "' and clave = sha1('" . $this->clave . "')";
    }

    function crear()
    {
        return "insert into domiciliario (nombre,apellido,correo,clave,estado)
                values ('" . $this->nombre . "', '" . $this->apellido . "', '" . $this->correo . "', '" . sha1($this->clave) . "', '" . $this->estado . "')";
    }

    function consultarTodos()
    {
        return "select idDomiciliario, nombre, apellido, correo,clave, foto, estado
                from domiciliario";
    }

    function consultar()
    {
        return "select nombre, apellido, correo, estado, foto,clave
                from domiciliario
                where idDomiciliario = '" . $this->idDomiciliario . "'";
    }

    public function editar()
    {
        return "update domiciliario
                set nombre = '" . $this->nombre . "', apellido = '" . $this->apellido .
            "', correo = '" . $this->correo .
            "' where idDomiciliario = '" . $this->idDomiciliario .  "'";
    }

    function verificar()
    {
        return "select * 
        from domiciliario 
        where correo='" . $this->correo . "'";
    }

    public function editarClave()
    {
        return "update domiciliario
                set clave = '" .  sha1($this->clave)  .
            "' where idDomiciliario = '" . $this->idDomiciliario .  "'";
    }

    function editarEstado()
    {
        return "update Domiciliario 
                set estado = '" . $this->estado . "'
                where idDomiciliario = '" . $this->idDomiciliario . "'";
    }

    function editarFoto()
    {
        return "update Domiciliario set foto = '" . $this->foto . "'
                where idDomiciliario = '" . $this->idDomiciliario . "'";
    }

    public function eliminarFoto()
    {
        return "update Domiciliario
                set foto = '' where idDomiciliario = '" . $this->idDomiciliario .  "'";
    }

    function consultarPorCorreo()
    {
        return "select idDomiciliario, nombre, apellido, correo
                from Domiciliario
                where correo = '" . $this->correo . "'";
    }

    function cambiarClave($id, $nuevaClave)
    {
        return "update Domiciliario set clave = sha1('" . $nuevaClave . "')
                where idDomiciliario = '" . $id . "'";
    }
}
