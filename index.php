<?php

session_start();

require "logica/Administrador.php";
require "logica/Cliente.php";
require "logica/Domiciliario.php";
require "logica/Inspector.php";
require "logica/Categoria.php";
require "logica/Producto.php";
require "logica/ProductoCategoria.php";
require "logica/ProductoTipo.php";
require "logica/Foto.php";
require "logica/Log.php";
require "persistencia/Conexion.php";
require "logica/Tipo.php";

function getVisitorIp()
{
    // Recogemos la IP de la cabecera de la conexión
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ipAdress = $_SERVER['HTTP_CLIENT_IP'];
    }
    // Caso en que la IP llega a través de un Proxy
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ipAdress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    // Caso en que la IP lleva a través de la cabecera de conexión remota
    else {
        $ipAdress = $_SERVER['REMOTE_ADDR'];
    }
    return $ipAdress;
}

$user_agent = $_SERVER['HTTP_USER_AGENT'];

function getBrowser($user_agent)
{

    if (strpos($user_agent, 'MSIE') !== FALSE)
        return 'Internet explorer';
    elseif (strpos($user_agent, 'Edge') !== FALSE) //Microsoft Edge
        return 'Microsoft Edge';
    elseif (strpos($user_agent, 'Trident') !== FALSE) //IE 11
        return 'Internet explorer';
    elseif (strpos($user_agent, 'Opera Mini') !== FALSE)
        return "Opera Mini";
    elseif (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR') !== FALSE)
        return "Opera";
    elseif (strpos($user_agent, 'Firefox') !== FALSE)
        return 'Mozilla Firefox';
    elseif (strpos($user_agent, 'Chrome') !== FALSE)
        return 'Google Chrome';
    elseif (strpos($user_agent, 'Safari') !== FALSE)
        return "Safari";
    else
        return 'No hemos podido detectar su navegador';
}

$navegador = getBrowser($user_agent);

$user_agent = $_SERVER['HTTP_USER_AGENT'];


function getPlatform($user_agent)
{
    $plataformas = array(
        'Windows 10' => 'Windows NT 10.0+',
        'Windows 8.1' => 'Windows NT 6.3+',
        'Windows 8' => 'Windows NT 6.2+',
        'Windows 7' => 'Windows NT 6.1+',
        'Windows Vista' => 'Windows NT 6.0+',
        'Windows XP' => 'Windows NT 5.1+',
        'Windows 2003' => 'Windows NT 5.2+',
        'Windows' => 'Windows otros',
        'iPhone' => 'iPhone',
        'iPad' => 'iPad',
        'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
        'Mac otros' => 'Macintosh',
        'Android' => 'Android',
        'BlackBerry' => 'BlackBerry',
        'Linux' => 'Linux',
    );
    foreach ($plataformas as $plataforma => $pattern) {
        if (preg_match('/(?i)' . $pattern . '/', $user_agent))
            return $plataforma;
    }
    return 'Otras';
}

$SO = getPlatform($user_agent);

$key = "i";

function encrypt($string, $key)
{
    $result = '';
    for ($i = 0; $i < strlen($string); $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key)) - 1, 1);
        $char = chr(ord($char) + ord($keychar));
        $result .= $char;
    }
    return base64_encode($result);
}

function decrypt($string, $key)
{
    $result = '';
    $string = base64_decode($string);
    for ($i = 0; $i < strlen($string); $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key)) - 1, 1);
        $char = chr(ord($char) - ord($keychar));
        $result .= $char;
    }
    return $result;
}

if (isset($_GET["sesion"]) && $_GET["sesion"] == 0) {
    unset($_SESSION["id"]);
    unset($_SESSION["rol"]);
    session_destroy();
    header("Location: index.php");
    exit();
}

$pid = null;
if (isset($_GET["pid"])) {
    $pid = decrypt($_GET["pid"], $key);
}

$webPages = array(
    "presentacion/administrador/sesionAdministrador.php",
    "presentacion/cliente/sesionCliente.php",
    "presentacion/administrador/producto/crearProducto.php",
    "presentacion/administrador/producto/consultarProducto.php",
    "presentacion/home.php",
    "presentacion/administrador/categoria/crearCategoria.php",
    "presentacion/administrador/categoria/consultarCategoria.php",
    "presentacion/administrador/categoria/editarCategoria.php",
    "presentacion/administrador/tipo/consultarTipo.php",
    "presentacion/administrador/tipo/crearTipo.php",
    "presentacion/administrador/categoria/updateCategoria.php",
    "presentacion/administrador/tipo/editarTipo.php",
    "presentacion/administrador/cliente/consultarCliente.php",
    "presentacion/administrador/domiciliario/crearDomiciliario.php",
    "presentacion/domiciliario/sesionDomiciliario.php",
    "presentacion/administrador/domiciliario/consultarDomiciliario.php",
    "presentacion/administrador/inspector/crearInspector.php",
    "presentacion/inspector/sesionInspector.php",
    "presentacion/administrador/inspector/consultarInspector.php",
    "presentacion/cliente/editarPerfil.php",
    "presentacion/cliente/car.php",
    "presentacion/producto.php",
);

$pagSinSesion = array(
    "presentacion/olvidoClave.php",
    "presentacion/crearCuenta.php",
    "presentacion/cliente/car.php",
    "presentacion/autenticar.php",
    "presentacion/producto.php",
);

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
    <link href="https://bootswatch.com/4/cyborg/bootstrap.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/carousel.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">

    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.8/js/dataTables.fixedHeader.min.js  "></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js "></script>

    <link rel="stylesheet" href="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1569006288/BBBootstrap/choices.min.css?version=7.0.0">
    <script src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1569006273/BBBootstrap/choices.min.js?version=7.0.0"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bootpag/1.0.4/jquery.bootpag.min.js"></script>

    <script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@3.5.14/dist/jspdf.plugin.autotable.js"> </script>

    <script src="https://cdn.tiny.cloud/1/qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc/tinymce/5/tinymce.min.js"></script>

    <title>IZIX</title>
    <link rel="icon" type="image/png" href="img/icon.png">
</head>

<body class="contenedor bg-white">

    <?php
    if (isset($pid)) {
        echo $pid;
        if (isset($_SESSION["id"])) {
            if ($_SESSION["rol"] == "Administrador") {
                include "presentacion/administrador/menuAdministrador.php";
            }
            if ($_SESSION["rol"] == "Cliente") {
                include "presentacion/Homemenu.php";
            }
            if ($_SESSION["rol"] == "Domiciliario") {
                include "presentacion/domiciliario/menuDomiciliario.php";
            }
            if ($_SESSION["rol"] == "Inspector") {
                include "presentacion/inspector/menuInspector.php";
            }
            if (in_array($pid, $webPages)) {
                include $pid;
            } else {
                include "presentacion/noExiste.php";
            }
        } else if (in_array($pid, $pagSinSesion)) {
            include $pid;
        } else {

            header("Location: index.php");
        }
    } else {
        if (isset($_SESSION["id"])) {
            include "presentacion/homeMenu.php";
        }
        include "presentacion/home.php";
    }

    if (isset($_SESSION["id"])) {
        if (($_SESSION["rol"] != "Administrador") && $_SESSION["rol"] != "Inspector") {
            include "presentacion/footer.php";
        }
    } else {
        include "presentacion/footer.php";
    }

    ?>

</body>

</html>