<?php
require "persistencia/LogDAO.php";

class Log{
    private $idLog;
    private $accion;
    private $datos;
    private $fechayhora;
    private $ip;
    private $os;
    private $navegador;
    private $Administrador_idAdministrador;
    private $conexion;
    private $LogDAO;
    
    public function getIdLog()
    {
        return $this->idLog;
    }

    public function getAccion()
    {
        return $this->accion;
    }

    public function getDatos()
    {
        return $this->datos;
    }

    public function getFechayhora()
    {
        return $this->fechayhora;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function getOs()
    {
        return $this->os;
    }

    public function getNavegador()
    {
        return $this->navegador;
    }

    public function getAdministrador_idAdministrador()
    {
        return $this->Administrador_idAdministrador;
    }

    function Log ($pIdLog="", $pAccion="", $pDatos="", $pFechayhora="",$pIp="",$pOs="", $pNavegador="", $pAdministrador_idAdministrador="") {
        $this -> idLog = $pIdLog;
        $this -> accion = $pAccion;
        $this -> datos = $pDatos;
        $this -> fechayhora = $pFechayhora;
        $this -> ip = $pIp;
        $this -> os = $pOs;
        $this -> navegador = $pNavegador;
        $this -> Administrador_idAdministrador = $pAdministrador_idAdministrador;
        $this -> conexion = new Conexion();
        $this -> LogDAO = new LogDAO($pIdLog, $pAccion, $pDatos, $pFechayhora, $pIp, $pOs, $pNavegador,$pAdministrador_idAdministrador);        
    }
    
    function consultar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> LogDAO -> consultar());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> idLog = $resultado[0];
        $this -> accion = $resultado[1];
        $this -> datos = $resultado[2];
        $this -> fechayhora = $resultado[3];
        $this -> ip = $resultado[4];
        $this -> os = $resultado[5];
        $this -> navegador = $resultado[6];
        $this -> Administrador_idAdministrador = $resultado[7];
    }

    function consultarPoraccion(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> LogDAO -> consultarPoraccion());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> idLog = $resultado[0];
    }

    function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> LogDAO -> crear());
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogDAO -> consultarTodos());
        $this -> conexion -> cerrar();
        $Logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Logs, new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]));
        }
        return $Logs;
    }
    
    function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogDAO -> editar());
        $this -> conexion -> cerrar();
    }

    function editardatos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogDAO -> editardatos());
        $this -> conexion -> cerrar();
    }

    function consultarPorPagina($fechayhora, $pagina, $orden, $dir){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogDAO -> consultarPorPagina($fechayhora, $pagina, $orden, $dir));
        $this -> conexion -> cerrar();
        $Loges = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Loges, new Log($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Loges;
    }
    
    function consultarTotalRegistros(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogDAO -> consultarTotalRegistros());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();        
        return $resultado[0];
    }
    
    function buscar($filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> LogDAO -> buscar($filtro));
        $this -> conexion -> cerrar();
        $Loges = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Loges, new Log($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Loges;
    }
    
}


?>