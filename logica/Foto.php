<?php
require "persistencia/FotoDAO.php";

class Foto{
    private $idFoto;
    private $foto;
    private $Producto_idProducto;
    private $conexion;
    private $FotoDAO;
    

    
    /**
     * @return string
     */
    public function getIdFoto()
    {
        return $this->idFoto;
    }

    /**
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @return string
     */
    public function getProducto_idProducto()
    {
        return $this->Producto_idProducto;
    }

    function Foto ($pIdFoto="", $pFoto="", $pProducto_idProducto="") {
        $this -> idFoto = $pIdFoto;
        $this -> foto = $pFoto;
        $this -> Producto_idProducto = $pProducto_idProducto;
        $this -> conexion = new Conexion();
        $this -> FotoDAO = new FotoDAO($pIdFoto, $pFoto, $pProducto_idProducto);        
    }
    
    function consultar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> FotoDAO -> consultar());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> foto = $resultado[0];
        $this -> Producto_idProducto = $resultado[1];
    }

    function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> FotoDAO -> crear());
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> FotoDAO -> consultarTodos());
        $this -> conexion -> cerrar();
        $Fotos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Fotos, new Foto($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Fotos;
    }

    function mostrarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> FotoDAO -> mostrarTodos());
        $this -> conexion -> cerrar();
        $Fotos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Fotos, new Foto($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Fotos;
    }
    
    function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> FotoDAO -> editar());
        $this -> conexion -> cerrar();
    }

    function editarProducto_idProducto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> FotoDAO -> editarProducto_idProducto());
        $this -> conexion -> cerrar();
    }

    function consultarPorPagina($cantidad, $pagina, $orden, $dir){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> FotoDAO -> consultarPorPagina($cantidad, $pagina, $orden, $dir));
        $this -> conexion -> cerrar();
        $Fotoes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Fotoes, new Foto($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Fotoes;
    }
    
    function consultarTotalRegistros(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> FotoDAO -> consultarTotalRegistros());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();        
        return $resultado[0];
    }
    
    function buscar($filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> FotoDAO -> buscar($filtro));
        $this -> conexion -> cerrar();
        $Fotoes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Fotoes, new Foto($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Fotoes;
    }
    
}


?>