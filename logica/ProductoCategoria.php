<?php
require "persistencia/ProductoCategoriaDAO.php";

class ProductoCategoria{
    private $Producto_idProducto;
    private $Categoria_idCategoria;
    private $conexion;
    private $ProductoCategoriaDAO;
    
    public function getProducto_idProducto()
    {
        return $this->Producto_idProducto;
    }

    public function getCategoria_idCategoria()
    {
        return $this->Categoria_idCategoria;
    }


    function ProductoCategoria ($pProducto_idProducto="", $pCategoria_idCategoria="") {
        $this -> Producto_idProducto = $pProducto_idProducto;
        $this -> Categoria_idCategoria = $pCategoria_idCategoria;
        $this -> conexion = new Conexion();
        $this -> ProductoCategoriaDAO = new ProductoCategoriaDAO($pProducto_idProducto, $pCategoria_idCategoria);        
    }
    
    function consultar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> ProductoCategoriaDAO -> consultar());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> Producto_idProducto = $resultado[0];
        $this -> Categoria_idCategoria = $resultado[1];
    }

    function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> ProductoCategoriaDAO -> crear());
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoCategoriaDAO -> consultarTodos());
        $this -> conexion -> cerrar();
        $ProductoCategorias = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($ProductoCategorias, new ProductoCategoria($resultado[0], $resultado[1]));
        }
        return $ProductoCategorias;
    }
    
    function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoCategoriaDAO -> editar());
        $this -> conexion -> cerrar();
    }

    function editarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoCategoriaDAO -> editarEstado());
        $this -> conexion -> cerrar();
    }
    
    function consultarTotalRegistros(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoCategoriaDAO -> consultarTotalRegistros());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();        
        return $resultado[0];
    }
    
}


?>