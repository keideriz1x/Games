<?php
require "persistencia/ProductoDAO.php";

class Producto{
    private $idProducto;
    private $nombre;
    private $precio;
    private $cantidad;
    private $video;
    private $descripcion;
    private $conexion;
    private $ProductoDAO;
    
    public function getidProducto()
    {
        return $this->idProducto;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getPrecio()
    {
        return $this->precio;
    }

    public function getCantidad()
    {
        return $this->cantidad;
    }

    public function getVideo()
    {
        return $this->video;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    function Producto ($pidProducto="", $pNombre="", $pPrecio="", $pCantidad="",$pVideo="",$pDescripcion="") {
        $this -> idProducto = $pidProducto;
        $this -> nombre = $pNombre;
        $this -> precio = $pPrecio;
        $this -> cantidad = $pCantidad;
        $this -> video = $pVideo;
        $this -> descripcion = $pDescripcion;
        $this -> conexion = new Conexion();
        $this -> ProductoDAO = new ProductoDAO($pidProducto, $pNombre, $pPrecio, $pCantidad, $pVideo, $pDescripcion);        
    }
    
    function consultar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> ProductoDAO -> consultar());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> idProducto = $resultado[0];
        $this -> nombre = $resultado[1];
        $this -> precio = $resultado[2];
        $this -> cantidad = $resultado[3];
        $this -> video = $resultado[4];
        $this -> descripcion = $resultado[5];
    }

    function consultarPorNombre(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> ProductoDAO -> consultarPorNombre());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> idProducto = $resultado[0];
    }

    function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> ProductoDAO -> crear());
        $this -> conexion -> cerrar();
    }

    function verificar () {
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> verificar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 0){
            return true;
        }else{
            return false;
        }
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> consultarTodos());
        $this -> conexion -> cerrar();
        $Productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Productos, new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]));
        }
        return $Productos;
    }
    
    function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> editar());
        $this -> conexion -> cerrar();
    }

    function editarprecio(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> editarprecio());
        $this -> conexion -> cerrar();
    }

    function consultarPorPagina($cantidad, $pagina, $orden, $dir){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> consultarPorPagina($cantidad, $pagina, $orden, $dir));
        $this -> conexion -> cerrar();
        $Productoes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Productoes, new Producto($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Productoes;
    }
    
    function consultarTotalRegistros(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ProductoDAO -> consultarTotalRegistros());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();        
        return $resultado[0];
    }
    
    function buscar($filtro){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> ProductoDAO -> buscar($filtro));
        $this -> conexion -> cerrar();
        $Productoes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Productoes, new Producto($resultado[0], $resultado[1], $resultado[2]));
        }
        return $Productoes;
    }
    
}


?>