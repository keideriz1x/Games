<?php
if (!isset($_SESSION["id"])) {
    include "presentacion/homeMenu.php";
}   
?>
<div class="col-md-8 container">
    <?php
    include "presentacion/carousel.php";
    ?>
    <div class="col-xs-12 pt-2">
        <div class="input-group">
            <div class="input-group-prepend my-auto">
                <h5 class="text-light">IZ1X</h5>
            </div>
            <div class="form-group col my-auto">
                <hr style="background-color:#f80000">
            </div>
            <span class="input-group-prepend my-auto">
                <div class="holder">
                </div>
            </span>
        </div>
    </div>
    <?php
    include "presentacion/vistaProductos.php";
    ?>

</div>