<?php

if (isset($_GET["idCliente"]) && isset($_GET["estado"])) {

	$idCliente = $_GET["idCliente"];
	$estado = $_GET["estado"];

	echo "<div id='accion" . $idCliente . "'><button class='btn btn-sm' id='cl" . $idCliente . "'>" . (($estado == 1) ? "<span class='fas fa-ban text-light' data-toggle='tooltip' title='Deshabilitar'></span>" : (($estado == 0) ? "<span class='fas fa-check text-light' data-toggle='tooltip' title='Habilitar'></span>" : "")) . "</button></div>";
}
?>
<?php
if (isset($_GET["idCliente"]) && isset($_GET["estado"])) { ?>
	<script>
		$(document).ready(function() {
			$("#cl<?php echo $idCliente ?>").click(function(e) {

				e.preventDefault();

				Swal.fire({
					title: '¿Seguro?',
					text: "¿Desea cambiar el estado de este cliente?",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Si, hacer cambio',
					cancelButtonText: "Cancelar"
				}).then((result) => {
					if (result.isConfirmed) {

						$('[data-toggle="tooltip"]').tooltip('hide');

						var url = "indexAjax.php?pid=<?php echo encrypt("presentacion/administrador/cliente/estadoClienteAjax.php", $key) ?>&idCliente=<?php echo $idCliente ?>&estado=<?php echo (($estado == 1) ? "0" : "1") ?>";
						$("#icono<?php echo $idCliente ?>").load(url);
						var url = "indexAjax.php?pid=<?php echo encrypt("presentacion/administrador/cliente/estadoAccionAjax.php", $key) ?>&idCliente=<?php echo $idCliente ?>&estado=<?php echo (($estado == 1) ? "0" : "1") ?>";
						$("#accion<?php echo $idCliente ?>").load(url);

					}
				})

			});
		});
	</script>
<?php
}
?>