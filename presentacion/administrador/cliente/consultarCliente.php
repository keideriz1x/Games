<?php
$cliente = new Cliente();
$clientes = $cliente->consultarTodos();
?>

<div class="container pb-1">
  <div class="row mt-4 justify-content-center d-flex">
    <div class="col-lg-12">
      <div class="card text-white pt-4">
        <div class="cardAdmin card-header text-center rounded">
          <h3>Consultar Cliente</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive contenedor">
            <table id="example-table" class="display" style="width:100%;color: black;">
              <thead style="color: white;">
                <th> Nombre</th>
                <th> Correo</th>
                <th> Estado</th>
                <th> Servicios</th>
              </thead>
              <tbody>

                <?php

                foreach ($clientes as $clienteActual) {
                  echo "<tr>";
                  echo "<td>" . $clienteActual->getNombre() . " " . $clienteActual->getApellido() . "</td>";
                  echo "<td>" . $clienteActual->getCorreo() . "</td>";
                  echo "<td>" . (($clienteActual->getEstado() == 1) ? "<div id='icono" . $clienteActual->getIdCliente() . "' class='text-light'>Activo</div>" : (($clienteActual->getEstado() == 0) ? "<div id='icono" . $clienteActual->getIdCliente() . "' class='text-light'>Restringido</div>" : "Inactivo")) . "</td>";
                  echo "<td>";
                  echo "<div id='accion" . $clienteActual->getIdCliente() . "'><button class='btn btn-sm' id='cl" . $clienteActual->getIdCliente() . "'>" . (($clienteActual->getEstado() == 1) ? "<span class='fas fa-ban text-light' data-toggle='tooltip' title='Deshabilitar'></span>" : (($clienteActual->getEstado() == 0) ? "<span class='fas fa-check text-light' data-toggle='tooltip' title='Habilitar'></span>" : "")) . "</button></div>";
                  echo "</td>";
                  echo "</tr>";
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php
  foreach ($clientes as $clienteActual) { ?>
    <script>
      $(document).ready(function() {
        $("#cl<?php echo $clienteActual->getIdCliente() ?>").click(function(e) {

          e.preventDefault();

          Swal.fire({
            title: '¿Seguro?',
            text: "¿Desea cambiar el estado de este cliente?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, hacer cambio',
            cancelButtonText: "Cancelar"
          }).then((result) => {
            if (result.isConfirmed) {


              $('[data-toggle="tooltip"]').tooltip('hide');
              var url = "indexAjax.php?pid=<?php echo encrypt("presentacion/administrador/cliente/estadoClienteAjax.php", $key) ?>&idCliente=<?php echo $clienteActual->getIdCliente() ?>&estado=<?php echo (($clienteActual->getEstado() == 1) ? "0" : "1") ?>";
              $("#icono<?php echo $clienteActual->getIdCliente() ?>").load(url);
              var url = "indexAjax.php?pid=<?php echo encrypt("presentacion/administrador/cliente/estadoAccionAjax.php", $key) ?>&idCliente=<?php echo $clienteActual->getIdCliente() ?>&estado=<?php echo (($clienteActual->getEstado() == 1) ? "0" : "1") ?>";
              $("#accion<?php echo $clienteActual->getIdCliente() ?>").load(url);

            }
          })

        });
      });
    </script>
  <?php
  }
  ?>

  <script>
    $(document).ready(function() {

      mostrar();

    });

    function mostrar() {
      var table = $('#example-table').DataTable({
        "sort": true,
        "destroy": true,
        "lengthMenu": [
          [5, 10, 25, 50, -1],
          [5, 10, 25, 50, "All"]
        ],
        "columnDefs": [{
          "targets": 3,
          "orderable": false
        }, ],
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        },
      });
      new $.fn.dataTable.FixedHeader(table);

    }
  </script>