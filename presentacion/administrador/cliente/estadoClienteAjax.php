<?php
if (isset($_GET["idCliente"]) && isset($_GET["estado"])) {

    $cliente = new Cliente($_GET["idCliente"], "", "", "", "", $_GET["estado"], "");
    $cliente->editarEstado();

    $consultar = new Cliente($_GET["idCliente"], "", "");
    $consultar->consultar();

    $log = new Log("", "Editar estado de Cliente", "id: ".$consultar->getIdCliente()." nombre: ".$consultar->getNombre()." ".$consultar->getApellido() . " cambiado a: " . (($_GET["estado"] == 1) ? "Activo" : "Restringido"), date("Y-m-d H:i:s"), getVisitorIp(), $SO, $navegador, $_SESSION["id"]);
    $log->crear();

    echo (($_GET["estado"] == 1) ? "<div id='icono" . $_GET["idCliente"] . "' class='text-light'>Activo</div>" : "<div id='icono" . $_GET["idCliente"] . "' class='text-light'>Restringido</div>");
}
