<?php
$producto = new Producto();
$productos = $producto->consultarTodos();

?>

<div class="container pb-1">
  <div class="row mt-4">
    <div class="col-lg-12">
      <div class="card text-white pt-4">
        <div class="cardAdmin card-header text-center rounded">
          <h3>Consultar Producto</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive contenedor">
            <table id="example-table" class="display nowrap" style="width:100%;color: black;">
              <thead style="color: white;">
                <th> Nombre</th>
                <th> Precio</th>
                <th> Cantidad</th>
                <th> Categoria</th>
                <th> Consola</th>
                <th>Servicios</th>
              </thead>
              <tbody>
                <?php
                foreach ($productos as $productoActual) {
                  $productocategoria = new ProductoCategoria($productoActual->getIdProducto(), "");
                  $productocategorias = $productocategoria->consultarTodos();
                  $productotipo = new ProductoTipo($productoActual->getIdProducto(), "");
                  $productotipos = $productotipo->consultarTodos();
                  echo "<tr>";
                  echo "<td>" . $productoActual->getNombre() . "</td>";
                  echo "<td>" . $productoActual->getPrecio() . "</td>";
                  echo "<td>" . $productoActual->getCantidad() . "</td>";
                  echo "<td>";
                  foreach ($productocategorias as $productocategoriaActual) {
                    $categoria = new Categoria($productocategoriaActual->getCategoria_idCategoria(), "", "");
                    $categorias = $categoria->mostrarTodos();
                    foreach ($categorias as $categoriaActual) {
                      echo $categoriaActual->getNombre();
                    }
                    if ($productocategoriaActual === end($productocategorias)) {
                      echo ".";
                    } else {
                      echo ", ";
                    }
                  }
                  echo "</td>";
                  echo "<td>";
                  foreach ($productotipos as $productotipoActual) {
                    $tipo = new Tipo($productotipoActual->getTipo_idTipo(), "", "");
                    $tipos = $tipo->mostrarTodos();
                    foreach ($tipos as $tipoActual) {
                      echo $tipoActual->getNombre();
                    }
                    if ($productotipoActual === end($productotipos)) {
                      echo ".";
                    } else {
                      echo ", ";
                    }
                  }
                  echo "</td>";
                  echo '<td> 
                  <button data-toggle="modal" data-target="#ver' . $productoActual->getIdProducto() . '" name="verMas" title="Ver más" class="btn btn-sm"><i class="fas fa-eye text-light"></i></button>
                  <button data-toggle="modal" data-target="#editar' . $productoActual->getIdProducto() . '" name="editarProducto" title="Editar Producto" class="btn btn-sm"><i class="fas fa-edit text-light"></i></button>
                  </td>';
                  echo "</tr>";
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
foreach ($productos as $productoActual) { ?>
  <div class="modal fade" id="ver<?php echo $productoActual->getIdProducto() ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">
    <div class="modal-dialog modal-lg modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title text-break" id="exampleModalLabel"><?php echo $productoActual->getNombre() ?></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row spacing-15">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div id="carouselExampleControls<?php echo $productoActual->getIdProducto() ?>" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">

                  <?php
                  $foto = new Foto("", "", $productoActual->getIdProducto());
                  $fotos = $foto->mostrarTodos();
                  $contador = 0;
                  $i = 1;
                  if (!empty($fotos) and count($fotos) > 0) {
                    foreach ($fotos as $fotoActual) { ?>

                      <?php if ($contador == 0) { ?>
                        <li data-target="#carouselExampleControls<?php echo $productoActual->getIdProducto() ?>" data-slide-to="0" class="active"></li>
                      <?php $contador = 1;
                      } else { ?>
                        <li data-target="#carouselExampleControls<?php echo $productoActual->getIdProducto() ?>" data-slide-to="<?php echo $i++ ?>"></li>
                      <?php   } ?>

                  <?php           }
                  }
                  ?>

                </ol>
                <div class="contentCarrusel carousel-inner">

                  <?php
                  $foto = new Foto("", "", $productoActual->getIdProducto());
                  $fotos = $foto->mostrarTodos();
                  $contador = 0;
                  if (!empty($fotos) and count($fotos) > 0) {
                    foreach ($fotos as $fotoActual) { ?>

                      <?php if ($contador == 0) { ?>
                        <div class="carousel-item active">

                          <img src="data:image/png;base64,<?php echo base64_encode($fotoActual->getFoto()) ?>" class="carrusel d-block">

                        </div>
                      <?php $contador = 1;
                      } else { ?>
                        <div class="carousel-item">

                          <img src="data:image/png;base64,<?php echo base64_encode($fotoActual->getFoto()) ?>" class=" carrusel d-block">

                        </div>
                      <?php   } ?>

                  <?php           }
                  }
                  ?>

                </div>
                <?php
                $foto = new Foto("", "", $productoActual->getIdProducto());
                $fotos = $foto->mostrarTodos();
                if (!empty($fotos) and count($fotos) > 1) { ?>
                  <a class="carousel-control-prev" href="#carouselExampleControls<?php echo $productoActual->getIdProducto() ?>" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleControls<?php echo $productoActual->getIdProducto() ?>" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                <?php
                }
                ?>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-white">
              <div class="table-responsive">
                <table class="table table-borderless table-hover">
                  <tr>
                    <th>Precio:</th>
                    <td><?php echo $productoActual->getPrecio() ?></td>
                  </tr>
                  <tr>
                    <th>Cantidad:</th>
                    <td><?php echo $productoActual->getCantidad() ?></td>
                  </tr>
                  <tr>
                    <th>Categoria:</th>
                    <td><?php
                        foreach ($productocategorias as $productocategoriaActual) {
                          $categoria = new Categoria($productocategoriaActual->getCategoria_idCategoria(), "", "");
                          $categorias = $categoria->mostrarTodos();
                          foreach ($categorias as $categoriaActual) {
                            echo $categoriaActual->getNombre();
                          }
                          if ($productocategoriaActual === end($productocategorias)) {
                            echo ".";
                          } else {
                            echo ", ";
                          }
                        }
                        ?></td>
                  </tr>
                  <tr>
                    <th>Consola:</th>
                    <td><?php
                        foreach ($productotipos as $productotipoActual) {
                          $tipo = new Tipo($productotipoActual->getTipo_idTipo(), "", "");
                          $tipos = $tipo->mostrarTodos();
                          foreach ($tipos as $tipoActual) {
                            echo $tipoActual->getNombre();
                          }
                          if ($productotipoActual === end($productotipos)) {
                            echo ".";
                          } else {
                            echo ", ";
                          }
                        }
                        ?></td>
                  </tr>
                  <tr>
                    <th>Video:</th>
                    <td>
                      <button data-toggle="modal" data-target="#video<?php echo $productoActual->getIdProducto() ?>" class="btn btn-sm"><i class="fab fa-youtube text-white" title="Ver video"></i></button>

                      <div class="modal" id="video<?php echo $productoActual->getIdProducto() ?>">
                        <div class="modal-dialog modal-xl modal-dialog-centered">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title text-break"><?php echo $productoActual->getNombre() ?></h4>
                              <button type="button" class="close" data-dismiss="modal">×</button>
                            </div>
                            <div class="container"></div>
                            <div class="modal-body contenedor">
                              <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $productoActual->getVideo() ?>" allowfullscreen></iframe>
                                <script>
                                  $(document).ready(function() {
                                    $('#video<?php echo $productoActual->getIdProducto() ?>').on('hidden.bs.modal', function() {
                                      var $this = $(this).find('iframe'),
                                        tempSrc = $this.attr('src');
                                      $this.attr('src', "");
                                      $this.attr('src', tempSrc);
                                    });

                                  });
                                </script>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
              <div class="text-center"><b>Descripcion</b></div>
              <div>
                <?php echo $productoActual->getDescripcion() ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php
}
?>

<script>
  $(document).ready(function() {
    window.addEventListener("resize", mostrar);
    mostrar();

  });

  function mostrar() {
    var table = $('#example-table').DataTable({
      "destroy": true,
      "lengthMenu": [
        [5, 10, 25, 50, -1],
        [5, 10, 25, 50, "All"]
      ],
      responsive: true,
      columnDefs: [{
          responsivePriority: 1,
          targets: 0
        },
        {
          responsivePriority: 10001,
          targets: 1
        },
        {
          responsivePriority: 2,
          targets: -1
        }
      ],
      "columnDefs": [{
        "targets": 5,
        "orderable": false
      }, ],
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
    new $.fn.dataTable.FixedHeader(table);
  }
</script>