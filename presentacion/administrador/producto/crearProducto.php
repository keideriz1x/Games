<?php

function getYoutubeEmbedUrl($url)
{
	$shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_]+)\??/i';
	$longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';

	if (preg_match($longUrlRegex, $url, $matches)) {
		$youtube_id = $matches[count($matches) - 1];
	}

	if (preg_match($shortUrlRegex, $url, $matches)) {
		$youtube_id = $matches[count($matches) - 1];
	}
	return $youtube_id;
}

if (isset($_POST["create_producto"])) {
	if ($_POST["nombre"] != "" && $_POST["precio"] != "" && $_POST["cantidad"] != "" && $_POST["descripcion"] != "" && $_FILES["file"] != "" && $_POST["video"] != "" && $_POST["categoria"] != "" && $_POST["tipo"] != "") {
		$pro = new Producto("", $_POST["nombre"]);

		if ($pro->verificar()) {
			$producto = new Producto("", $_POST["nombre"], $_POST["precio"], $_POST["cantidad"], getYoutubeEmbedUrl($_POST["video"]), $_POST["descripcion"]);
			$producto->crear();
			$id = new Producto("", $_POST["nombre"]);
			$id->consultarPorNombre();
			$categorias = $_POST["categoria"];
			$i = 0;
			while ($i < count($categorias)) {
				$productocategoria = new ProductoCategoria($id->getidProducto(), $categorias[$i++]);
				$productocategoria->crear();
			}
			$tipos = $_POST["tipo"];
			$j = 0;
			while ($j < count($tipos)) {
				$productotipo = new ProductoTipo($id->getidProducto(), $tipos[$j++]);
				$productotipo->crear();
			}
			$files = $_FILES["file"]['tmp_name'];
			foreach ($files as $key) {
				$imgContent = addslashes(file_get_contents($key));
				$foto = new Foto("", $imgContent, $id->getidProducto());
				$foto->crear();
			}

			$log = new Log("", "Crear producto", "Producto: " . $_POST["nombre"], date("Y-m-d H:i:s"), getVisitorIp(), $SO, $navegador, $_SESSION["id"]);
			$log->crear();

			echo "<script>
	Swal.fire({
		icon: 'success',
		title: 'Producto creado con exito.',
		showConfirmButton: false,
		timer: 1500
	})
</script>";
		} else {
			echo "<script>
				Swal.fire({
					icon: 'error',
					title: 'Producto ya existe.',
				})
			</script>";
		}
	} else {
		echo "<script>
			Swal.fire({
				icon: 'error',
				title: 'Por favor ingrese todos los datos.',
			})
		</script>";
	}
}



?>
<div class="container pb-1">
	<div class="row mt-4">
		<div class="col-3"></div>
		<div class="col-lg-6">
			<div class="card pt-4">
				<div class="cardAdmin card-header text-center text-white rounded">
					<h3>Crear Producto</h3>
				</div>
				<div class="card-body text-white">
					<form id="create_producto" enctype="multipart/form-data" action=<?php echo "index.php?pid=" . encrypt("presentacion/administrador/producto/crearProducto.php", $key) ?> onsubmit="return validar()" method="post">

						<div class="form-group">
							<input type="text" maxlength="70" minlength="5" id="nombre" name="nombre" class="form-control" placeholder="Nombre*" required="required" autocomplete="off">
						</div>
						<div class="form-group">
							<input type="text" maxlength="10" minlength="4" class="form-control" id="precio" name="precio" placeholder="Precio*" required="required" autocomplete="off" pattern="[0-9]*" title="Formato numerico">
						</div>
						<div class="form-group">
							<input type="text" maxlength="2" class="validanumericos form-control" id="stock" name="cantidad" placeholder="Cantidad*" required="required" autocomplete="off" pattern="[0-9]*" title="Formato numerico">
						</div>

						<div class="form-group">
							<textarea class="form-control descripcion" placeholder="Descripcion*" name="descripcion" id="descripcion"></textarea>
						</div>

						<div class="form-group">
							<div class="input-group">
								<input type="text" class="form-control" readonly>
								<div class="input-group-append">
									<span class="fileUpload btn btn-danger" style="z-index: 0;">
										<span class="upl" id="upload">Subir imagenes*</span>
										<input type="file" class="upload up" name="file[]" id="files" accept="image/*" multiple required />
									</span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<input type="url" class="form-control" name="video" placeholder="Enlace de youtube*" required pattern="http://www\.youtube\.com\/(.+)|https://www\.youtube\.com\/(.+)" autocomplete="off" />
						</div>

						<div class="fom-group mb-2">

							<div class="row d-flex justify-content-center">
								<div class="col-md-12">

									<select name="categoria[]" id="categoria" placeholder="Seleccione maximo 3 categorias*" multiple>

										<?php
										$categoria = new Categoria();
										$categorias = $categoria->consultarTodos();
										foreach ($categorias as $categoriaActual) {
											if ($categoriaActual->getEstado() == 1) {
												echo "<option value='" . $categoriaActual->getIdCategoria() . "'>" . $categoriaActual->getNombre() . "</option>";
											}
										}
										?>
									</select>
								</div>
							</div>

						</div>

						<div class="fom-group mb-2">

							<div class="row d-flex justify-content-center">
								<div class="col-md-12">
									<select id="tipo" name="tipo[]" placeholder="Seleccione maximo 3 consolas*" multiple>
										<?php
										$tipo = new Tipo();
										$tipos = $tipo->consultarTodos();
										foreach ($tipos as $tipoActual) {
											if ($tipoActual->getEstado() == 1) {
												echo "<option value='" . $tipoActual->getIdTipo() . "'>" . $tipoActual->getNombre() . "</option>";
											}
										}
										?>
									</select>
									</select>
								</div>
							</div>

						</div>

						<div class="form-group text-center">
							<button type="submit" id="crear" name="create_producto" class="btn btn-danger btn-block">Crear</button>
						</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function validar() {
		//obteniendo el valor que se puso en campo text del formulario
		tipo = document.getElementById("tipo").value;
		categoria = document.getElementById("categoria").value;
		descripcion = document.getElementById("descripcion").value;
		//la condición
		if (tipo.length == 0 && categoria.length == 0 && descripcion.length == 0) {
			Swal.fire({
				icon: 'error',
				title: 'Por ingrese todos los datos.',
			});
			return false;
		}
		return true;
	}
</script>
<script>
	$(document).on('change', '.up', function() {
		var names = [];
		var length = $(this).get(0).files.length;
		for (var i = 0; i < $(this).get(0).files.length; ++i) {
			names.push($(this).get(0).files[i].name);
		}
		// $("input[name=file]").val(names);
		if (length > 1) {
			var fileName = names.join(', ');
			$(this).closest('.form-group').find('.form-control').attr("value", length + " archivos seleccionados");
		} else {
			$(this).closest('.form-group').find('.form-control').attr("value", names);
		}
	});
</script>
<script>
	$(document).ready(function() {
		var multipleCancelButton = new Choices('#categoria', {
			removeItemButton: true,
			maxItemCount: 3,
			searchResultLimit: 3,
		});
	});
</script>

<script>
	$(document).ready(function() {
		var multipleCancelButton = new Choices('#tipo', {
			removeItemButton: true,
			maxItemCount: 3,
			searchResultLimit: 3,
		});
	});
</script>
<script>
	tinymce.init({
		selector: '.descripcion',
		menubar: false,
		statusbar: false,
		language: 'es',
		toolbar: 'undo redo | formatselect | ' +
			'bold italic backcolor | alignleft aligncenter ' +
			'alignright alignjustify | bullist numlist outdent indent | ' +
			'removeformat',
	});
</script>