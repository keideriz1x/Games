<?php
$administrador = new Administrador($_SESSION["id"]);
$administrador->consultar();
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="index.php?pid=<?php echo encrypt("presentacion/administrador/sesionAdministrador.php", $key) ?>"><img src="img/logo.png" width="100px"></a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Producto
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo encrypt("presentacion/administrador/producto/crearProducto.php",$key) ?>">Crear</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo encrypt("presentacion/administrador/producto/consultarProducto.php",$key) ?>">Consultar</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categoria y Tipo
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo encrypt("presentacion/administrador/categoria/crearCategoria.php",$key) ?>">Crear Categoria</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo encrypt("presentacion/administrador/categoria/consultarCategoria.php",$key) ?>">Consultar Categoria</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo encrypt("presentacion/administrador/tipo/crearTipo.php",$key) ?>">Crear Tipo</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo encrypt("presentacion/administrador/tipo/consultarTipo.php",$key) ?>">Consultar Tipo</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Usuarios
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo encrypt("presentacion/administrador/cliente/consultarCliente.php",$key) ?>">Consultar Cliente</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo encrypt("presentacion/administrador/domiciliario/crearDomiciliario.php",$key) ?>">Crear Domiciliario</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo encrypt("presentacion/administrador/domiciliario/consultarDomiciliario.php",$key) ?>">Consultar Domiciliario</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo encrypt("presentacion/administrador/inspector/crearInspector.php",$key) ?>">Crear Inspector</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo encrypt("presentacion/administrador/inspector/consultarInspector.php",$key) ?>">Consultar Inspector</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          PDF
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/admin/permisoAdmin.php") ?>&priv=<?php echo base64_encode("presentacion/admin/pdf/generarPDF.php") ?>">Reporte</a>
        </div>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/admin/permisoAdmin.php") ?>&priv=<?php echo base64_encode("presentacion/admin/log/log.php") ?>">Log</a>
      </li>
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item active d-none d-lg-none d-xl-block my-auto">

        <?php echo $administrador->getFoto() == null ? '<i class="fas fa-user-circle fa-2x my-auto" style="color: white;"></i>':'<img src="data:image/png;base64,'. base64_encode($administrador->getFoto()).'" class="menuPerfil my-auto">'?>

      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Administrador: <?php echo $administrador->getNombre() . " " . $administrador->getApellido() ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?sesion=0">Cerrar Sesión</a>
        </div>
      </li>
    </ul>
  </div>

</nav>