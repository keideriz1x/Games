<?php
$error = 0;
if (isset($_GET["error"])) {
    $error = $_GET["error"];
}
?>
<?php
if (isset($_SESSION["id"])) {
    $cliente = new Cliente($_SESSION["id"]);
    $cliente->consultar();
}
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="col-3 order-0 order-lg-0 order-md-0 d-flex justify-content-lg-center">
        <div class="my-auto">
            <button class="navbar-toggler px-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="my-auto">
            <a class="navbar-brand" href="index.php"><img src="img/logo.png" style="width: 100px;"></a>
        </div>
    </div>
    <div class="col-lg-6 order-2 order-lg-1 order-md-2">
        <div class="mx-auto d-block w-100">
            <div class="input-group">
                <div class="input-group-prepend">
                    <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span style="font-size: calc(0.9% + 0.9em)">Categoria</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <?php
                        $categoria = new Categoria();
                        $categorias = $categoria->consultarTodos();
                        foreach ($categorias as $categoriaActual) {
                            if ($categoriaActual->getEstado() == 1) {
                        ?>
                                <a class="dropdown-item" href="#"><?php echo $categoriaActual->getNombre() ?></a>
                        <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
                <input type="text" class="form-control" aria-label="Text input with dropdown button">
                <span class="input-group-prepend">
                    <button class="btn btn-danger" type="button"><i class="fas fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>
    <div class="col-3 order-1 order-lg-2 order-md-1 d-inline-flex justify-content-center ">
        <?php
        if (isset($_SESSION["id"]) && $_SESSION["rol"] == "Cliente") { ?>
            <a class="d-flex text-white text-decoration-none">
                <?php
                if ($cliente->getFoto() == null) {
                ?>
                    <i class="fas fa-user-circle fa-2x my-auto" style="color: white;"></i>
                <?php
                } else {
                ?>
                    <img src="data:image/png;base64,<?php echo base64_encode($cliente->getFoto()) ?>" class="menuPerfil my-auto">
                <?php
                }
                ?>

                <div class=" ml-2 my-auto d-none d-md-none d-lg-block text-nowrap"><?php echo $cliente->getNombre() . " " . $cliente->getApellido();
                                                                                    ?></div>
            </a>
        <?php
        } else {
        ?>
            <a href="?" data-toggle="modal" data-target="#exampleModal" class="d-flex text-white text-decoration-none">
                <i class="far fa-user fa-lg my-auto"></i>
                <div class=" ml-2 my-auto d-none d-md-none d-lg-block text-nowrap">Mi cuenta</div>
            </a>
        <?php
        }
        ?>
        <hr style="border-left: 2px dotted red;height:30px;width:1px" class="mx-1">
        <a href="<?php echo "index.php?pid=" . encrypt("presentacion/cliente/car.php", $key) ?>" class=" d-flex text-white text-decoration-none">
            <svg xmlns="http://www.w3.org/2000/svg" style="border-radius: 8px;z-index: 1;" width="1.7em" height="1.7em" fill="currentColor" class="bi bi-cart3 my-auto position-relative overflow-hidden" viewBox="0 0 16 16">
                <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
            </svg>
            <span class="text-white text-nowrap bg-danger text-center position-relative" style="font-size: 10px;line-height: 20px;border-radius: 50%;min-width: 20px;height: 20px;right: 1em;top: 1.5em;z-index: 3;">0</span>
        </a>
    </div>
</nav>

<nav class="navbar navbar-expand-lg navbar-dark bg-danger">
    <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
        <?php
        $tipo=new Tipo();
        $tipos=$tipo->consultarTodos();
        foreach ($tipos as $tipoActual) {
            if ($tipoActual->getEstado()==1) {
            ?>
           <div class="text-white mx-4"><a class="text-decoration-none text-white" href=""><?php echo $tipoActual->getNombre()?></a></div>
            <?php
}
        }
        ?>

        <?php
        if (isset($_SESSION["id"]) && $_SESSION["rol"] == "Cliente") { ?>
            <div class="mx-4 "><a class="text-decoration-none text-white" href=""><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-handbag-fill pb-1 d-none d-md-none d-lg-inline" viewBox="0 0 16 16">
                        <path d="M8 1a2 2 0 0 0-2 2v2H5V3a3 3 0 0 1 6 0v2h-1V3a2 2 0 0 0-2-2zM5 5H3.361a1.5 1.5 0 0 0-1.483 1.277L.85 13.13A2.5 2.5 0 0 0 3.322 16h9.356a2.5 2.5 0 0 0 2.472-2.87l-1.028-6.853A1.5 1.5 0 0 0 12.64 5H11v1.5a.5.5 0 0 1-1 0V5H6v1.5a.5.5 0 0 1-1 0V5z" />
                    </svg><b>PEDIDOS</b></a></div>
            <ul class="navbar-nav mx-4">
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle p-0 text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-person-square mb-1 d-none d-md-none d-lg-inline" viewBox="0 0 16 16">
                            <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                            <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm12 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1v-1c0-1-1-4-6-4s-6 3-6 4v1a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12z" />
                        </svg> <b>PERFIL</b>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo "index.php?pid=" . encrypt("presentacion/cliente/editarPerfil.php", $key) ?>">Editar Perfil</a>
                        <a class="dropdown-item" href="index.php?sesion=0">Cerrar Sesión</a>
                    </div>
                </li>
            </ul>
        <?php
        } ?>
    </div>
</nav>

<div class="modal fade pt-5" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header d-block">
                <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title text-center" id="exampleModalLabel">Autenticar</h5>
            </div>
            <div class="modal-body">
                <form action=<?php echo "index.php?pid=" . encrypt("presentacion/autenticar.php", $key) ?> method="post">
                    <div class="form-group">
                        <input type="email" name="correo" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" class="form-control" placeholder="Correo" required="required">
                    </div>
                    <div class="form-group">
                        <input type="password" name="clave" minlength="4" maxlength="15" class="form-control" placeholder="Clave" required="required">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-outline-danger btn-block">Iniciar sesion</button>
                    </div>
                    <p class="text-center">
                        <a href="<?php echo "index.php?pid=" . encrypt("presentacion/olvidoClave.php", $key) ?>" class="text-danger">¿Olvido su contraseña?</a>
                    </p>
                    <p class="text-center">
                        <a>o</a>
                    </p>
                    <p class="text-center">
                        <a href="<?php echo "index.php?pid=" . encrypt("presentacion/crearCuenta.php", $key) ?>" class="text-danger">Crear una cuenta</a>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>
<?php if ($error == 1) { ?>
    <script>
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Usuario y/o contraseña incorrecto',
        })
    </script>
<?php } else if ($error == 2) { ?>
    <<script>
        Swal.fire({
        icon: 'warning',
        text: 'Usuario deshabilitado',
        })
        </script>
    <?php } ?>