<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.carousel.min.css'>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.theme.default.min.css'>
<link rel="stylesheet" type="text/css" href="css/carousel.css">
<script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js'></script>
<div id="sync1" class="owl-carousel owl-theme">
    <div class="item">
        <img src="https://fondosmil.com/fondo/3602.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://i.pinimg.com/originals/fc/b2/6f/fcb26f4597f93f67a4dc9a5b9c734925.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://www.wallpapertip.com/wmimgs/80-804998_videojuegos-hd.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://i.pinimg.com/originals/db/80/25/db802545cb920e77f02f3ffd53e0d57d.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://www.10wallpaper.com/wallpaper/medium/1909/Resident_Evil_2_2019_Game_HD_Poster_medium.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://www.10wallpaper.com/wallpaper/medium/2004/Pubg_2020_Mobile_Game_4K_HD_Poster_medium.jpg" alt="">
    </div>
    <div class="item">
        <img src=" https://www.wallpaperk.com/wallpapers/destiny-poster-5093.jpg" alt="">
    </div>
    <div class="item">
        <img src=" https://10wallpaper.com/wallpaper/medium/2004/Valorant_Closed_Beta_2020_Game_HD_Poster_medium.jpg" alt="">
    </div>
</div>

<div id="sync2" class="owl-carousel owl-theme">
    <div class="item">
        <img src="https://fondosmil.com/fondo/3602.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://i.pinimg.com/originals/fc/b2/6f/fcb26f4597f93f67a4dc9a5b9c734925.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://www.wallpapertip.com/wmimgs/80-804998_videojuegos-hd.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://i.pinimg.com/originals/db/80/25/db802545cb920e77f02f3ffd53e0d57d.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://www.10wallpaper.com/wallpaper/medium/1909/Resident_Evil_2_2019_Game_HD_Poster_medium.jpg" alt="">
    </div>
    <div class="item">
        <img src="https://www.10wallpaper.com/wallpaper/medium/2004/Pubg_2020_Mobile_Game_4K_HD_Poster_medium.jpg" alt="">
    </div>
    <div class="item">
        <img src=" https://www.wallpaperk.com/wallpapers/destiny-poster-5093.jpg" alt="">
    </div>
    <div class="item">
        <img src=" https://10wallpaper.com/wallpaper/medium/2004/Valorant_Closed_Beta_2020_Game_HD_Poster_medium.jpg" alt="">
    </div>
</div>
<script>
    $(document).ready(function() {

        var sync1 = $("#sync1");
        var sync2 = $("#sync2");
        var slidesPerPage = 5;
        var syncedSecondary = true;

        sync1.owlCarousel({
            animateOut: 'fadeOutDown',
            animateIn: 'fadeInUp',
            items: 1,
            slideSpeed: 2000,
            nav: true,
            autoplay: true,
            dots: true,
            loop: true,
            responsiveRefreshRate: 200,
            navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
        }).on('changed.owl.carousel', syncPosition);

        sync2
            .on('initialized.owl.carousel', function() {
                sync2.find(".owl-item").eq(0).addClass("current");
            })
            .owlCarousel({
                items: slidesPerPage,
                dots: false,
                nav: false,
                smartSpeed: 200,
                slideSpeed: 500,
                slideBy: slidesPerPage,
                responsiveRefreshRate: 100
            }).on('changed.owl.carousel', syncPosition2);

        function syncPosition(el) {

            var count = el.item.count - 1;
            var current = Math.round(el.item.index - (el.item.count / 2) - .5);

            if (current < 0) {
                current = count;
            }
            if (current > count) {
                current = 0;
            }

            //end block

            sync2
                .find(".owl-item")
                .removeClass("current")
                .eq(current)
                .addClass("current");
            var onscreen = sync2.find('.owl-item.active').length - 1;
            var start = sync2.find('.owl-item.active').first().index();
            var end = sync2.find('.owl-item.active').last().index();

            if (current > end) {
                sync2.data('owl.carousel').to(current, 100, true);
            }
            if (current < start) {
                sync2.data('owl.carousel').to(current - onscreen, 100, true);
            }
        }

        function syncPosition2(el) {
            if (syncedSecondary) {
                var number = el.item.index;
                sync1.data('owl.carousel').to(number, 100, true);
            }
        }

        sync2.on("click", ".owl-item", function(e) {
            e.preventDefault();
            var number = $(this).index();
            sync1.data('owl.carousel').to(number, 300, true);
        });
    });
</script>