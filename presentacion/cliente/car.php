<?php
if (!isset($_SESSION["id"])) {
    include "presentacion/homeMenu.php";
}   
?>
<div class='container'>
    <div class='row mt-4'>

        <div class='col-lg-12'>
            <div class='card pt-4'>
                <div class='cardCl card-header text-center text-white rounded'>
                    <h3>Carrito de compras</h3>
                </div>

                <div class='card-body'>
                    <div class='contenedor table-responsive'>
                        <table class="table table-striped">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Precio unidad</th>
                                    <th scope="col">Cantidad</th>
                                    <th scope="col">Precio Total </th>
                                    <th scope="col">Servicios</th>
                                </tr>
                            </thead>
                            <tbody id="productos_carrito">
                            </tbody>
                        </table>

                    </div>
                    <div class="d-flex justify-content-between pt-3">
                        <a type='button' class='btn btn-danger col-md-2' onclick='history.back()'>Regresar</a>

                        <a type='button' id="continuar" href="index.php?pid=<?php echo base64_encode("presentacion/client/permisoClient.php") ?>&priv=<?php echo base64_encode("presentacion/client/compra.php") ?>" class='btn btn-success float-right col-md-2'>Continuar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
