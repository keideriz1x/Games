<?php
$domiciliario = new Domiciliario($_SESSION["id"]);
$domiciliario->consultar();
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand px" href="index.php?pid=<?php echo encrypt("presentacion/domiciliario/sesionDomiciliario.php", $key) ?>"><img src="img/logo.png" width="100px"></a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/dom/permisoDom.php") ?>&priv=<?php echo base64_encode("presentacion/dom/pedido/elegirPedidos.php") ?>">Elegir pedido</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/dom/permisoDom.php") ?>&priv=<?php echo base64_encode("presentacion/dom/pedido/historialPedidos.php") ?>">Historial de pedidos </a>
      </li>
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item active d-none d-md-none d-lg-block my-auto">

        <?php
        if ($domiciliario->getFoto() == null) {
        ?>
          <i class="fas fa-user-circle fa-2x my-auto" style="color: white;"></i>
        <?php
        } else {
        ?>
          <img src="data:image/png;base64,<?php echo base64_encode($domiciliario->getFoto()) ?>" class="menuPerfil my-auto">
        <?php
        }
        ?>

      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Domiciliario: <?php echo $domiciliario->getNombre() . " " . $domiciliario->getApellido() ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/dom/permisoDom.php") ?>&priv=<?php echo base64_encode("presentacion/editPerfil.php") ?>">Editar Perfil</a>
          <a class="dropdown-item" href="index.php?sesion=0">Cerrar Sesión</a>
        </div>
      </li>
    </ul>
  </div>

</nav>