<?php
$inspector = new Inspector($_SESSION["id"]);
$inspector->consultar();
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand px" href="index.php?pid=<?php echo encrypt("presentacion/inspector/sesionInspector.php",$key) ?>&priv=<?php echo base64_encode("presentacion/inspect/sesionInspect.php") ?>"><img src="img/logo.png" width="100px"></a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Pedidos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/inspect/permisoInspect.php") ?>&priv=<?php echo base64_encode("presentacion/inspect/pedido/consultarPedido.php") ?>">Consultar</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/inspect/permisoInspect.php") ?>&priv=<?php echo base64_encode("presentacion/inspect/pedido/validarPedido.php") ?>">Validar</a>
        </div>
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Estadisticas
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/inspect/permisoInspect.php") ?>&priv=<?php echo base64_encode("presentacion/inspect/estadisticas/juegosConsola.php") ?>">Juegos por consola</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/inspect/permisoInspect.php") ?>&priv=<?php echo base64_encode("presentacion/inspect/estadisticas/juegosCategoria.php") ?>">Juegos por categoria</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/inspect/permisoInspect.php") ?>&priv=<?php echo base64_encode("presentacion/inspect/estadisticas/juegosComprados.php") ?>">Juegos comprados</a>
        </div>
      </li>
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item active d-none d-md-none d-lg-block my-auto">

        <?php
        if ($inspector->getFoto() == null) {
        ?>
          <i class="fas fa-user-circle fa-2x my-auto" style="color: white;"></i>
        <?php
        } else {
        ?>
          <img src="data:image/png;base64,<?php echo base64_encode($inspector->getFoto()) ?>" class="menuPerfil my-auto">
        <?php
        }
        ?>
        <!-- Foto aqui, tamaño de foto de 2em -->
      </li>
      <li class="nav-item dropdown active">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Inspector: <?php echo $inspector->getNombre() . " " . $inspector->getApellido() ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/inspect/permisoInspect.php") ?>&priv=<?php echo base64_encode("presentacion/editPerfil.php") ?>">Editar Perfil</a>
          <a class="dropdown-item" href="index.php?sesion=0">Cerrar Sesión</a>
        </div>
      </li>
    </ul>
  </div>

</nav>