<?php
$inspector = new Inspector($_SESSION["id"]);
$inspector->consultar();

if (isset($_POST["editar"])) {
	if (($_POST["nombre"] != "") && ($_POST["apellido"] != "") && ($_POST["correo"] != "")) {
		$inspector = new Inspector($_SESSION["id"], $_POST["nombre"], $_POST["apellido"], $_POST["correo"], "", "");
		$inspector->editar();
		echo "<script>
		Swal.fire({
			icon: 'success',
			title: 'Informacion editada con exito.',
			showConfirmButton: false,
			timer: 1500
		})
	</script>";
	}
}

if (isset($_POST["editarc"])) {
	if ($_POST["nueva"] != "" && $_POST["nNueva"] != "" && $_POST["antigua"] != "") {
		if ($_POST["nueva"] == $_POST["nNueva"]) {
			if ($inspector->getClave() == (sha1($_POST["antigua"]))) {
				$admin = new Inspector($_SESSION["id"], "", "", "", $_POST["nueva"], "");
				$admin->editarClave();
				echo "<script>
				Swal.fire({
					icon: 'success',
					title: 'Contraseña editada con exito.',
					showConfirmButton: false,
					timer: 1500
				})
			</script>";
			} else {
				echo "<script>
				Swal.fire({
					icon: 'error',
					title: 'Contraseña actual incorrecta.',
				})
			</script>";
			}
		} else {
			echo "<script>
			Swal.fire({
				icon: 'error',
				title: 'Contraseñas no coinciden.',
			})
		</script>";
		}
	}
}
if (isset($_POST["editarFoto"])) {
	if (isset($_FILES['foto']) && strlen($_FILES['foto']['tmp_name']) > 0) {
		$check = getimagesize($_FILES["foto"]["tmp_name"]);
		if ($check != false) {
			$foto = $_FILES['foto']['tmp_name'];
			$imgContent = addslashes(file_get_contents($foto));

			$adminFoto = new Inspector($_SESSION["id"], "", "", "", "", $imgContent);
			$adminFoto->editarFoto();
			echo "<script>
		Swal.fire({
			icon: 'success',
			title: 'Foto editada con exito.',
			showConfirmButton: false,
			timer: 1500
		})
	</script>";
		} else {
			echo "<script>
			Swal.fire({
				icon: 'error',
				title: 'Por favor suba una imagen.',
			})
		</script>";
		}
	} else {
		echo "<script>
			Swal.fire({
				icon: 'error',
				title: 'Por favor suba una imagen.',
			})
		</script>";
	}
}

if (isset($_POST["eliminarFoto"])) {

			$aFoto = new Inspector($_SESSION["id"], "", "", "", "", "");
			$aFoto->eliminarFoto();
			echo "<script>
		Swal.fire({
			icon: 'success',
			title: 'Foto eliminada con exito.',
			showConfirmButton: false,
			timer: 1500
		})
	</script>";		
}

$inspector->consultar();

?>

<div class="container pb-1">
	<div class="row mt-4 justify-content-center d-flex">
		<div class="col-lg-8">
			<div class="card text-white pt-4">
				<div class="cardAdmin card-header text-center rounded">
					<h3>Bienvenido</h3>
				</div>
				<div class="card-body">
					<div class="row">
						<div id="fotoAjax" class="col-sm-3">
							<?php echo $inspector->getFoto() == null ? '<img src="img/profile.jpeg" width="100%" class="my-auto">' : '<img src="data:image/png;base64,' . base64_encode($inspector->getFoto()) . '" width="100%" class="my-auto">' ?>
							<div class="d-flex justify-content-around pt-3">
								<button data-toggle="modal" data-target="#foto" name="editarFoto" title="Editar Foto" class="btn btn-sm btn-danger"><i class="fas fa-camera"></i></button>

								<button data-toggle="modal" data-target="#eliminar" onclick="pulsar()" name="eliminarFoto" title="Eliminar Foto" class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button>
							</div>
						</div>
						<div class="col-sm-9 pt-4">
							<div class="table-responsive">
								<table class="table table-borderless table-hover">
									<tr>
										<th>Nombre:</th>
										<td><?php echo $inspector->getNombre() ?></td>
									</tr>
									<tr>
										<th>Apellido:</th>
										<td><?php echo $inspector->getApellido() ?></td>
									</tr>
									<tr>
										<th>Correo:</th>
										<td><?php echo $inspector->getCorreo() ?></td>
									</tr>
								</table>
							</div>
						</div>

					</div>
				</div>
				<div class="card-footer py-1" style="text-align: right;">
					<button data-toggle="modal" data-target="#Informacion" style="text-align: right;" class="btn text-danger" title="Editar perfil"><i class="fas fa-user-edit"> </i></button>
					<button data-toggle="modal" data-target="#Clave" style="text-align: right;" class="btn text-danger" title="Editar contraseña"><i class="fas fa-unlock-alt"> </i></button>

				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal edicion foto-->

<div class="modal fade" id="foto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Editar Foto</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form action="index.php?pid=<?php echo encrypt("presentacion/inspector/sesionInspector.php", $key) ?>" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<div class="input-group">
							<input type="text" class="form-control" readonly>
							<div class="input-group-append">
								<span class="fileUpload btn btn-danger" style="z-index: 0;">
									<span class="upl" id="upload">Subir imagenes</span>
									<input type="file" class="upload up" name="foto" id="files" accept="image/*" />
								</span>
							</div>
						</div>
					</div>
					<img class="pb-3 mx-auto d-block" width="40%" id="imagenPrevisualizacion">
					<button type="submit" id="editarFoto" name="editarFoto" class="btn btn-danger btn-block">Editar Foto</button>
				</form>
			</div>
		</div>
	</div>

</div>

<script>
	$(document).on('change', '.up', function() {
		var names = [];
		var length = $(this).get(0).files.length;
		for (var i = 0; i < $(this).get(0).files.length; ++i) {
			names.push($(this).get(0).files[i].name);
		}
		// $("input[name=file]").val(names);
		if (length > 1) {
			var fileName = names.join(', ');
			$(this).closest('.form-group').find('.form-control').attr("value", length + " archivos seleccionados");
		} else {
			$(this).closest('.form-group').find('.form-control').attr("value", names);
		}
	});
</script>

<script>
	const $seleccionArchivos = document.querySelector("#files"),
		$imagenPrevisualizacion = document.querySelector("#imagenPrevisualizacion");

	// Escuchar cuando cambie
	$seleccionArchivos.addEventListener("change", () => {
		// Los archivos seleccionados, pueden ser muchos o uno
		const archivos = $seleccionArchivos.files;
		// Si no hay archivos salimos de la función y quitamos la imagen
		if (!archivos || !archivos.length) {
			$imagenPrevisualizacion.src = "";
			return;
		}
		// Ahora tomamos el primer archivo, el cual vamos a previsualizar
		const primerArchivo = archivos[0];
		// Lo convertimos a un objeto de tipo objectURL
		const objectURL = URL.createObjectURL(primerArchivo);
		// Y a la fuente de la imagen le ponemos el objectURL
		$imagenPrevisualizacion.src = objectURL;
	});
</script>

<!-- Modal eliminacion foto-->

<div class="modal fade" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">¿Esta seguro de eliminar la foto?</h5>
			</div>
			<div class="modal-body">

				<form action="index.php?pid=<?php echo encrypt("presentacion/inspector/sesionInspector.php", $key) ?>" method="post" enctype="multipart/form-data">
					<div class="d-flex justify-content-around">
						<button type="submit" name="eliminarFoto" class="btn btn-danger">Si, Eliminar</i></button>

						<button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cancelar</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>

<!-- Modal edicion informacion -->
<div class="modal fade" id="Informacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Editar Informacion</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="index.php?pid=<?php echo encrypt("presentacion/inspector/sesionInspector.php", $key) ?>" method="post" enctype="multipart/form-data">
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">Nombre</span>
						</div>
						<input type="text" name="nombre" class="form-control" value="<?php echo $inspector->getNombre() ?>" required>
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">Apellido</span>
						</div>
						<input type="text" name="apellido" class="form-control" value="<?php echo $inspector->getApellido() ?>" required>
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">Correo</span>
						</div>
						<input type="text" name="correo" class="form-control" value="<?php echo $inspector->getCorreo() ?>" required>
					</div>
					<button type="submit" name="editar" class="btn btn-danger btn-block">Editar Informacion</button>
				</form>
			</div>
		</div>
	</div>

</div>

<!-- Modal edicion contraseña-->

<div class="modal fade" id="Clave" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Editar Contraseña</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form action="index.php?pid=<?php echo encrypt("presentacion/inspector/sesionInspector.php", $key) ?>" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<input type="password" placeholder="Contraseña Actual" name="antigua" class="form-control" required>
					</div>
					<div class="form-group">
						<input type="password" placeholder="Nueva Contraseña" id="clave" name="nueva" class="form-control" required>
					</div>
					<div class="form-group">
						<input type="password" placeholder="Repetir Nueva Contraseña" id="cClave" name="nNueva" class="form-control" required>
					</div>
					<button type="submit" name="editarc" class="btn btn-danger btn-block">Editar Contraseña</button>
				</form>
			</div>
		</div>
	</div>

</div>

<script>
	$(function() {
		$(document).on('keyup', '#clave, #cClave', function() {
			var clave = $('#clave').val().trim();
			var cClave = $('#cClave').val().trim();
			if (!clave || !cClave || clave == '' || cClave == '') {
				$('#clave').removeClass('is-valid');
				$('#cClave').removeClass('is-valid');
			} else {
				if (clave !== cClave) {
					$('#clave').removeClass('is-valid').addClass('is-invalid');
					$('#cClave').removeClass('is-valid').addClass('is-invalid');
				} else {
					$('#clave').removeClass('is-invalid').addClass('is-valid');
					$('#cClave').removeClass('is-invalid').addClass('is-valid');
				}
			}
		});
	});
</script>
