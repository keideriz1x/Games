<?php

if (isset($_POST["create_cuenta"])) {
    $cl = new Cliente("", "", "", $_POST["correo"]);
    if ($cl->verificar()) {
        if (($_POST["correo"] == $_POST["cCorreo"]) && ($_POST["clave"] == $_POST["cClave"])) {

            $cliente = new Cliente("", $_POST["nombre"], $_POST["apellido"], $_POST["correo"], $_POST["clave"], 1, "");
            $cliente->crear();
            echo "<script>
                Swal.fire({
                    icon: 'success',
                    title: 'Su cuenta ha sido creada con exito.',
                    showConfirmButton: false,
                    timer: 1500
                })
		</script>";
        } else {
            echo "<script>
            Swal.fire({
                icon: 'error',
                title: 'Contraseña o correo no coinciden.',
            })
		</script>";
        }
    } else {
        echo "<script>
			Swal.fire({
				icon: 'error',
				title: 'Este correo ya esta vinculado con una cuenta.',
			})
		</script>";
    }
}

include "presentacion/homeMenu.php";
?>

<div class="col-lg-4 container pt-5">
    <div class="card">
        <div class="cardAdmin card-header text-center text-white rounded">
            <h3>Crear Cuenta</h3>
        </div>
        <div class="card-body">
            <form id="crear" action=<?php echo "index.php?pid=" . encrypt("presentacion/crearCuenta.php", $key) ?> onsubmit="return validacion();" method="post">
                <div class="form-group">
                    <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre" required="required" autocomplete="off" maxlength="20" minlength="3">
                </div>
                <div class="form-group">
                    <input type="text" id="apellido" name="apellido" class="form-control" placeholder="Apellido" required="required" autocomplete="off" maxlength="20" minlength="3">
                </div>
                <div class="form-group">
                    <input type="email" id="correo" name="correo" class="form-control" placeholder="Correo electronico" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" size="30" required autocomplete="off">
                </div>
                <div class="form-group">
                    <input type="email" id="cCorreo" name="cCorreo" class="form-control" placeholder="Confirmar Correo electronico" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" size="30" required autocomplete="off">
                </div>
                <div class="form-group">
                    <input type="password" id="clave" name="clave" class="form-control redo" placeholder="Contraseña" required="required" autocomplete="off" minlength="4" maxlength="15">
                </div>
                <div class="form-group">
                    <input type="password" id="cClave" name="cClave" class="form-control redo" placeholder="Confirmar Contraseña" required="required" autocomplete="off" minlength="4" maxlength="15">
                </div>
                <div class="form-group text-center">

                    <button type="submit" name="create_cuenta" class="btn btn-danger btn-block">Crear cuenta</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script>
    $(function() {
        $(document).on('keyup', '#correo, #cCorreo', function() {
            var correo = $('#correo').val().trim();
            var cCorreo = $('#cCorreo').val().trim();
            if (!correo || !cCorreo || correo == '' || cCorreo == '') {
                $('#correo').removeClass('is-valid');
                $('#cCorreo').removeClass('is-valid');
            } else {
                if (correo !== cCorreo) {
                    $('#correo').removeClass('is-valid').addClass('is-invalid');
                    $('#cCorreo').removeClass('is-valid').addClass('is-invalid');
                } else {
                    $('#correo').removeClass('is-invalid').addClass('is-valid');
                    $('#cCorreo').removeClass('is-invalid').addClass('is-valid');
                }
            }
        });
    });
</script>
<script>
    $(function() {
        $(document).on('keyup', '#clave, #cClave', function() {
            var clave = $('#clave').val().trim();
            var cClave = $('#cClave').val().trim();
            if (!clave || !cClave || clave == '' || cClave == '') {
                $('#clave').removeClass('is-valid');
                $('#cClave').removeClass('is-valid');
            } else {
                if (clave !== cClave) {
                    $('#clave').removeClass('is-valid').addClass('is-invalid');
                    $('#cClave').removeClass('is-valid').addClass('is-invalid');
                } else {
                    $('#clave').removeClass('is-invalid').addClass('is-valid');
                    $('#cClave').removeClass('is-invalid').addClass('is-valid');
                }
            }
        });
    });
</script>
<script type="text/javascript">
    function validacion() {
        //obteniendo el valor que se puso en campo text del formulario
        var correo = $("#correo").val();
        var cCorreo = $("#cCorreo").val();
        var clave = $("#clave").val();
        var cClave = $("#cClave").val();
        //la condición
        if (((correo != cCorreo) || (clave != cClave)) || ((correo != cCorreo) && (clave != cClave))) {
            Swal.fire({
                icon: 'error',
                title: 'Contraseña o correo no coinciden.',
            });
            return false;
        }
        return true;
    }
</script>