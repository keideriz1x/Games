<?php
$correo = $_POST["correo"];
$clave = $_POST["clave"];

$administrador = new Administrador("", "", "", $correo, $clave);
$cliente = new Cliente("", "", "", $correo, $clave);
$domiciliario = new Domiciliario("", "", "", $correo, $clave);
$inspector = new Inspector("", "", "", $correo, $clave);
if ($administrador->autenticar()) {
    $_SESSION["id"] = $administrador->getIdAdministrador();
    $_SESSION["rol"] = "Administrador";
    header("Location: index.php?pid=" . encrypt("presentacion/administrador/sesionAdministrador.php", $key));
} else if ($cliente->autenticar()) {
    if ($cliente->getEstado() == 1) {
        $_SESSION["id"] = $cliente->getIdCliente();
        $_SESSION["rol"] = "Cliente";
        header("Location: index.php?pid=" . encrypt("presentacion/home.php", $key));
    } else {
        header("Location: index.php?error=2");
    }
} else if ($domiciliario->autenticar()) {
    if ($domiciliario->getEstado() == 1) {
        $_SESSION["id"] = $domiciliario->getIdDomiciliario();
        $_SESSION["rol"] = "Domiciliario";
        header("Location: index.php?pid=" . encrypt("presentacion/domiciliario/sesionDomiciliario.php", $key));
    } else {
        header("Location: index.php?error=2");
    }
} else if ($inspector->autenticar()) {
    if ($inspector->getEstado() == 1) {
        $_SESSION["id"] = $inspector->getIdInspector();
        $_SESSION["rol"] = "Inspector";
        header("Location: index.php?pid=" . encrypt("presentacion/inspector/sesionInspector.php", $key));
    } else {
        header("Location: index.php?error=2");
    }
} else {
    header("Location: index.php?error=1");
}
