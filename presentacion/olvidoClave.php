<?php
include "presentacion/homeMenu.php";

?>
<div class="col-lg-5 container pt-5">
    <div class="card">
        <div class="card-header text-center text-white rounded">
            <h3>Recupera tu cuenta</h3>
        </div>
        <div class="card-body text-center">
            <form id="clave" action=<?php echo "index.php?pid=" . encrypt("presentacion/olvidoClave.php", $key) ?> method="post">
                <div class="pb-2">Ingresa tu correo electrónico para enviar codigo de cambio de contraseña</div>
                <div class="form-group">
                    <input type="email" maxlength="30" minlength="14" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" id="correo" class="form-control" placeholder="Correo electronico" required="required" autocomplete="off">
                </div>
                <div class="form-group text-center">
                    <button type="submit" id="enviar" class="btn btn-danger btn-block">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>